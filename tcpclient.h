﻿#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QWaitCondition>

class TcpClient : public QObject
{
    Q_OBJECT
public:
    explicit TcpClient(QObject *parent = 0);
    bool connect(const QHostAddress &address);
    void setWaitCondition(QWaitCondition *waitCondition);

signals:

public slots:
    void sendRequest(const QString &request);

private:
    QTcpSocket *m_socket;
    QWaitCondition *m_waitCondition;

};

#endif // TCPCLIENT_H
