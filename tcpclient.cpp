﻿#include <QTextStream>
#include <QThread>
#include "tcpclient.h"

TcpClient::TcpClient(QObject *parent) :
    QObject(parent)
  , m_socket(new QTcpSocket(this))
  , m_waitCondition(nullptr)
{
    QObject::connect(m_socket, &QTcpSocket::readyRead, this, [this]{
        QTextStream(stdout) << m_socket->bytesAvailable() << endl;
        QTextStream(stdout) << m_socket->readAll() << endl;
        QTextStream(stdout) << m_socket->bytesAvailable() << endl;
        m_waitCondition->wakeOne();
    });
}

bool TcpClient::connect(const QHostAddress &address)
{
    m_socket->connectToHost(address, 27220);
    if (!m_socket->waitForConnected(5000)) {
        QTextStream(stdout) << "connection failed." << endl;
        return false;
    }
    return true;
}

void TcpClient::setWaitCondition(QWaitCondition *waitCondition)
{
    m_waitCondition = waitCondition;
}

void TcpClient::sendRequest(const QString &request)
{
    qDebug() << "sendRequest: " << QThread::currentThreadId();
    m_socket->write(request.toLocal8Bit());
    m_socket->flush();
}
