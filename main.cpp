﻿#include <QCoreApplication>
#include <QHostAddress>
#include <QThread>
#include <QMutex>
#include <QMutexLocker>
#include <iostream>
#include "tcpclient.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    qDebug() << "main: " << QThread::currentThreadId();

    TcpClient client;
    QWaitCondition waitCondition;
    client.setWaitCondition(&waitCondition);
    if (!client.connect(QHostAddress::LocalHost)) {
        return 0;
    }
    QThread thread;
    client.moveToThread(&thread);
    thread.start();

    QMutex mutex;
    std::string line;
    forever {
        QMutexLocker locker(&mutex);
        std::cout << "Message: ";
        std::cin >> line;
        if (line == "q:") {
            break;
        }
        if (!QMetaObject::invokeMethod(&client, "sendRequest",
                                  Q_ARG(const QString &, QString(line.c_str())))) {
            qDebug() << "invokeMethod failed";
        }
        waitCondition.wait(&mutex);
    }

    return a.exec();
}
