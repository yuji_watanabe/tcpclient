#-------------------------------------------------
#
# Project created by QtCreator 2014-04-15T14:17:11
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = tcpclient
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    tcpclient.cpp

HEADERS += \
    tcpclient.h
